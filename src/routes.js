import React from 'react';
import { Route } from 'react-router-dom';

import HomePage from './pages/HomePage';
import DashboardPage from './pages/DashboardPage';
import PaymentsPage from './pages/PaymentsPage';
import NewPaymentIssuerPage from './pages/NewPaymentIssuerPage';
import GenerateSepaFilePage from './pages/GenerateSepaFilePage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';
import ForgotPasswordPage from './pages/ForgotPasswordPage';
import ResetPasswordPage from './pages/ResetPasswordPage';
import LogOutPage from './pages/LogOutPage';
import ConfirmEmailPage from './pages/ConfirmEmailPage';
import PrivateRoute from './components/common/PrivateRoute';

const Routes = ({ authenticated, checked }) => (
	<div>
		{ authenticated &&
			<div>
				<PrivateRoute exact path="/dashboard" component={DashboardPage} authenticated={authenticated} />
			  <PrivateRoute path="/payment_issuers/new" component={NewPaymentIssuerPage} authenticated={authenticated} />
			  <PrivateRoute path="/logout" component={LogOutPage} authenticated={authenticated} />
			  <PrivateRoute path="/payments" component={PaymentsPage} authenticated={authenticated} />
			  <PrivateRoute path="/sepa_file/new" component={GenerateSepaFilePage} authenticated={authenticated} />
			</div>
		}

		<div>
			<Route exact path="/" component={HomePage}/>
		  <Route path="/login" component={LoginPage} />
		  <Route path="/register" component={RegisterPage} />
		  <Route path="/confirm_email" component={ConfirmEmailPage} />
		  <Route path="/forgot_password" component={ForgotPasswordPage} />
		  <Route path="/reset_password" component={ResetPasswordPage} />
		</div>
	</div>
);

export default Routes;