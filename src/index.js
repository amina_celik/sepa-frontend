import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import store from './store/storeConfiguration';
import './styles/index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

require('dotenv').config();

render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
);

registerServiceWorker();
