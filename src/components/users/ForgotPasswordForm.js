import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { withRouter, Redirect } from 'react-router-dom';

import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Button from 'react-validation/build/button';

import { required } from '../../utils/Validators';

function mapStateToProps(state) {
	return {
		user: state.user
	}
}

function mapDispatchToProps(dispatch) {
	return {
		userActions: bindActionCreators(userActions, dispatch)
	};
};

class ConnectedForm extends Component {
	constructor() {
		super();

		this.state = {
			email: '',
			redirectToHome: false
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		});
	}

	handleSubmit(event) {
		event.preventDefault();
		this.form.validateAll();
		
		const user = Object.assign({}, this.state);
		this.props.userActions.forgotPassword({ user });
		
		this.setState({
			email: '',
			redirectToHome: true
		});
	}

	removeErrorMsg = () => {
		this.form.hideError(this.userInput);
	}

	render() {
		if(this.state.redirectToHome) {
			return <Redirect to="/" />;
		}
		else {
			return (
				<div className="row justify-content-center">
					<div className='col-md-6'>
						<h2>Forgot Password?</h2>
						<Form ref={ c => { this.form = c } } onSubmit={this.handleSubmit}>
							<div className="form-group">
								<Input 
									type='email' 
									name='email' 
									placeholder='*Your email address' 
									className='form-control' 
									value={this.state.email}
									onChange={this.handleChange} 
									onFocus={this.removeErrorMsg} 
									ref={c => { this.userInput = c }} 
									validations={[required]}
								/>
							</div>

							<Button type='submit' className='btn btn-dark btn-block'>Send reset link</Button>
						</Form>
					</div>
				</div>
			);
		}
	}
}

const ForgotPasswordForm = withRouter(connect(mapStateToProps, mapDispatchToProps)(ConnectedForm));

ForgotPasswordForm.propTypes = {
  userActions: PropTypes.object
};

export default ForgotPasswordForm;
