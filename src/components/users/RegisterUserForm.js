import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { withRouter, Redirect } from 'react-router-dom';

import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Button from 'react-validation/build/button';

import { required, password, passwordConfirmation } from '../../utils/Validators';

function mapStateToProps(state) {
	return {
		user: state.user
	}
}

function mapDispatchToProps(dispatch) {
	return {
		userActions: bindActionCreators(userActions, dispatch)
	};
};

class ConnectedForm extends Component {
	constructor() {
		super();

		this.state = {
			email: '',
			password: '',
			password_confirmation: '',
			redirectToLogin: false
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		});
	}

	handleSubmit(event) {
		event.preventDefault();
		this.form.validateAll();
		
		const user = Object.assign({}, this.state);
		this.props.userActions.createUser({ user });
		
		this.setState({
			email: '',
			password: '',
			password_confirmation: '',
			redirectToLogin: true // automatic redirect because backend does not raise errors on create new user
		});
	}

	removeErrorMsg = () => {
		this.form.hideError(this.userInput);
	}

	render() {
		if(this.state.redirectToLogin) {
			return <Redirect to="/login" />;
		}
		else {
			return (
				<div className="row justify-content-center">
					
					<div className='col-md-6'>
						<h2>Register</h2>
						<Form ref={ c => { this.form = c } } onSubmit={this.handleSubmit}>
							<div className="form-group">
								<Input 
									type='email' 
									name='email' 
									placeholder='*Your email address' 
									className='form-control' 
									value={this.state.email}
									onChange={this.handleChange} 
									onFocus={this.removeErrorMsg} 
									ref={c => { this.userInput = c }} 
									validations={[required]}
								/>
							</div>

							<div className="form-group">
								<Input 
									type='password' 
									name='password' 
									placeholder='*Your password' 
									className='form-control' 
									value={this.state.password}
									onChange={this.handleChange} 
									validations={[required, password, passwordConfirmation]} 
								/>
							</div>

							<div className="form-group">
								<Input 
									type='password' 
									name='password_confirmation' 
									placeholder='*Confirm your password' 
									className='form-control' 
									value={this.state.password_confirmation}
									onChange={this.handleChange} 
									validations={[required, passwordConfirmation]} 
								/>
							</div>

							<Button type='submit' className='btn btn-dark btn-block'>Register</Button>
						</Form>
					</div>
				</div>
			);
		}
	}
}

const RegisterUserForm = withRouter(connect(mapStateToProps, mapDispatchToProps)(ConnectedForm));

RegisterUserForm.propTypes = {
  userActions: PropTypes.object
};

export default RegisterUserForm;
