import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { withRouter, Link, Redirect } from 'react-router-dom';

import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Button from 'react-validation/build/button';
import ErrorMsg from '../user_notifications/ErrorMsg';

import { required } from '../../utils/Validators';

function mapStateToProps(state) {
	return {
		user: state.user,
		redirectToDashboard: state.session.authenticated
	}
}

function mapDispatchToProps(dispatch) {
	return {
		userActions: bindActionCreators(userActions, dispatch)
	};
};

class ConnectedForm extends Component {
	constructor() {
		super();

		this.state = {
			email: '',
			password: ''
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		});
	}

	handleSubmit(event) {
		event.preventDefault();
		this.form.validateAll();
		
		const user = Object.assign({}, this.state);
		this.props.userActions.loginUser(user);
	}

	removeErrorMsg = () => {
		this.form.hideError(this.userInput);
	}

	render() {
		if(this.props.redirectToDashboard) {
			return <Redirect to="/dashboard" />;
		}
		else {
			return (
				<div className="row justify-content-center">
					<ErrorMsg error={this.props.user.error} />

					<div className='col-md-6'>
						<h2>Log In</h2>
						<Form ref={ c => { this.form = c } } onSubmit={this.handleSubmit}>
							<div className="form-group">
								<Input 
									type='email' 
									name='email' 
									placeholder='*Your email address' 
									className='form-control' 
									value={this.state.email}
									onChange={this.handleChange} 
									onFocus={this.removeErrorMsg} 
									ref={c => { this.userInput = c }} 
									validations={[required]}
								/>
							</div>

							<div className="form-group">
								<Input 
									type='password' 
									name='password' 
									placeholder='*Your password' 
									className='form-control' 
									value={this.state.password}
									onChange={this.handleChange} 
									validations={[required]} 
								/>
							</div>

							<Button type='submit' className='btn btn-dark btn-block'>Log In</Button>
						</Form>

						<Link to="/forgot_password" className="nav-link">Forgot password?</Link>
					</div>
				</div>
			);
		}
	}
}

const LoginForm = withRouter(connect(mapStateToProps, mapDispatchToProps)(ConnectedForm));

LoginForm.PropTypes = {
  userActions: PropTypes.object,
  user: PropTypes.object,
  redirectToDashboard: PropTypes.bool
};

export default LoginForm;
