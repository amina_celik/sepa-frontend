import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as userActions from '../../actions/userActions';
import { withRouter } from 'react-router-dom';

const queryString = require('query-string');

function mapStateToProps(state) {
	const emailState = state.user;
	return {
		runningConfirmation: emailState.runningConfirmation,
		confirmed: emailState.confirmed
	}
}

function mapDispatchToProps(dispatch) {
	return {
		userActions: bindActionCreators(userActions, dispatch)
	};
};

class ConnectedForm extends Component {
	componentWillMount() {
		const parsedParams = queryString.parse(this.props.location.search);
		const token = parsedParams.token;

		this.props.userActions.confirmEmail(token);
	}

	shouldComponentUpdate(nextProps) {
		return nextProps.confirmed;
	}

	render() {
		if(this.props.runningConfirmation) {
			return (
				<div className='col-md-6'>
					<div>Please wait...</div>
				</div>
			);
		}
		else {
			if(this.props.confirmed) {
				return (
					<div className='col-md-6'>
						<div>Thank you. Your email is confirmed!</div>
						<div>Please <a href="/login">Log In</a> using your email and password.</div>
					</div>
				);
			}
			else {
				return (
					<div className='col-md-6'>
						<div>There was an error while trying to confirm your email.</div>
					</div>
				);
			}
		}
	}
}

const ConfirmEmailForm = withRouter(connect(mapStateToProps, mapDispatchToProps)(ConnectedForm));

ConfirmEmailForm.PropTypes = {
  userActions: PropTypes.object,
  location: PropTypes.object,
  confirmed: PropTypes.bool,
  runningConfirmation: PropTypes.bool
};

export default ConfirmEmailForm;