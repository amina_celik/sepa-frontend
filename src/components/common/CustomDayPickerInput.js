import React from 'react';
import PropTypes from 'prop-types';

import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';

import { DateUtils } from 'react-day-picker';

import dateFnsFormat from 'date-fns/format';
import dateFnsParse from 'date-fns/parse';

function parseDate(str, format, locale) {
  const parsed = dateFnsParse(str, format, { locale });
  if (DateUtils.isDate(parsed)) {
    return parsed;
  }
  return undefined;
}

function formatDate(date, format, locale) {
  return dateFnsFormat(date, format, { locale });
}

const CustomDayPickerInput = (props) => {
  const FORMAT = 'DD-MM-YYYY';

  return (
  	<DayPickerInput 
  		id={props.id} 
  		onDayChange={(date) => props.onDayChange(dateFnsFormat(date, FORMAT))} 
  		formatDate={formatDate} 
  		format={FORMAT} 
  		parseDate={parseDate} 
  		placeholder={`${dateFnsFormat(new Date(), FORMAT)}`}
  	/>
  )
}

CustomDayPickerInput.propTypes = {
  id: PropTypes.string,
  onDayChange: PropTypes.func
};

export default CustomDayPickerInput;