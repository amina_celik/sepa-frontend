import React, { Component } from 'react';
import PropTypes from 'prop-types';

function getPages(total) {
  var i = 0;
  var pages = [];
  while(i < total) {
    pages.push(i);
    i++;
  }
  return pages;
}

class Pagination extends Component {
  render() {

    var totalCount = this.props.totalCount || 0;
    var perPage = this.props.perPage || 10;
    var currentPage = this.props.currentPage || 0;

    var pagesCount = Math.ceil(totalCount / perPage);
    var pages = getPages(pagesCount);

    return (
      <nav aria-label="Page navigation example">
        <ul className="pagination justify-content-center">
          <li className={"page-item" + ((currentPage === 1) ? " disabled" : "")}>
            <a className="page-link" aria-label="Previous" onClick={() => this.props.changePage(0)}>
              <span aria-hidden="true">&laquo;</span>
              <span className="sr-only">Previous</span>
            </a>
          </li>

          {
            pages.map((pageNumber) => {
              return (
                <li className={"page-item" + ((pageNumber === currentPage) ? " active" : "")} key={"page_" + pageNumber}>
                  <a className="page-link" onClick={() => this.props.changePage(pageNumber)}>{pageNumber + 1}</a>
                </li>
              )
            })
          }

          <li className={"page-item" + ((currentPage === pagesCount) ? " disabled" : "") }>
            <a className="page-link" aria-label="Next" onClick={() => this.props.changePage(pages[pages.length - 1])}>
              <span aria-hidden="true">&raquo;</span>
              <span className="sr-only">Next</span>
            </a>
          </li>
        </ul>
      </nav>
    )
  }
}

Pagination.propTypes = {
  totalCount: PropTypes.number,
  perPage: PropTypes.number,
  currentPage: PropTypes.number
};

export default Pagination;