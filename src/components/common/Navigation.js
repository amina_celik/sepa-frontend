import React from 'react';
import { Link } from 'react-router-dom';

const Navigation = ({ authenticated, checked }) => {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">

      {
        authenticated ?
          <a className="navbar-brand" href="/dashboard">SEPA APP</a>
        :
          <a className="navbar-brand" href="/">SEPA APP</a>
      }
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>

        {
          authenticated ?
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item active">
                  <Link to="/dashboard" className="nav-link">Dashboard<span className="sr-only">(current)</span></Link>
                </li>

                <li className="nav-item">
                  <Link to="/payments" className="nav-link">Payments</Link>
                </li>
              </ul>
              
              <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                  <Link to="/logout" className="nav-link">Log Out</Link>
                </li>
              </ul>
            </div>
          :
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                  <Link to="/login" className="nav-link">Log In</Link>
                </li>

                <li className="nav-item">
                  <Link to="/register" className="nav-link">Register</Link>
                </li>
              </ul>
            </div>
        }
    </nav>
  );
}

export default Navigation;