import React from 'react';
import PropTypes from 'prop-types';

const ValidationErrorMsg = (props) => {
  return (
  	<div className='validation-error'>{ props.msg }</div>
  )
}

ValidationErrorMsg.propTypes = {
  msg: PropTypes.string
};

export default ValidationErrorMsg;