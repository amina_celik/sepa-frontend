import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as paymentIssuersActions from '../../actions/paymentIssuersActions';
import { withRouter } from 'react-router-dom';

function mapStateToProps(state) {
	return { 
		paymentIssuers: state.paymentIssuers
	};
}

function mapDispatchToProps(dispatch) {
	return {
		paymentIssuersActions: bindActionCreators(paymentIssuersActions, dispatch)
	};
}

class ConnectedList extends Component {
	componentWillMount() {
		this.props.paymentIssuersActions.fetchPaymentIssuers();
	}

	renderData(item) {
		return (
			<li key={item.id} className="list-group-item">
				<div className="issuer-name">{item.full_name}</div>
				<div className="issuer-iban">{item.iban}</div>
				<div className="issuer-organization">{item.organization}</div>
			</li>
		);
	}

	render() {
		if(!this.props.paymentIssuers) {
			return (
				<div> Loading payment details ... </div>
			);
		}
		else {
			return (
				<div className="card">
				  <div className="card-header">
				    Payment Issuer Data Sets
				  </div>
				  <ul className="list-group list-group-flush">
				  	{
							this.props.paymentIssuers.map((item, index) => {
	              return (
	                this.renderData(item)
	              );
	            })
						}
				  </ul>
				</div>
			);
		}
	}
}

const PaymentDetailsList = withRouter(connect(mapStateToProps, mapDispatchToProps)(ConnectedList));

PaymentDetailsList.propTypes = {
  paymentIssuersActions: PropTypes.object,
  paymentIssuers: PropTypes.array
};

export default PaymentDetailsList;