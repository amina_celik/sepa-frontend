import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as paymentIssuerActions from '../../actions/paymentIssuerActions';

import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Button from 'react-validation/build/button';

import { required, iban, fullName } from '../../utils/Validators';
import { withRouter, Redirect } from 'react-router-dom';

function mapStateToProps(state) {
	return {
		paymentIssuers: state.paymentIssuers
	}
}

function mapDispatchToProps(dispatch) {
	return {
		paymentIssuerActions: bindActionCreators(paymentIssuerActions, dispatch)
	};
};

class ConnectedForm extends Component {
	constructor() {
		super();

		this.state = {
			full_name: '',
			iban: '',
			organization: '',
			address: '',
			location: '',
			model_number: '',
			reference_call: '',
			redirectToDashboard: false
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		});
	}

	handleSubmit(event) {
		event.preventDefault();
		this.form.validateAll();
		
		const issuer = Object.assign({}, this.state);
		this.props.paymentIssuerActions.createPaymentIssuer(issuer);
	}

	componentWillReceiveProps(newProps) {
		if(newProps.paymentIssuers.length === (this.props.paymentIssuers.length + 1))
			this.setState({
				full_name: '',
				iban: '',
				organization: '',
				address: '',
				location: '',
				model_number: '',
				reference_call: '',
				redirectToDashboard: true
			});
	}

	removeErrorMsg = () => {
		this.form.hideError(this.userInput);
	}

	render() {
		if(this.state.redirectToDashboard) {
			return <Redirect to="/dashboard" />;
		}
		else {
			return (
				<div className="row justify-content-center">
					<div className='col-md-6'>
						<h2>Create Payment Details</h2>

						<div style={{fontSize: '12px'}}>
							You must have at least one payment details set in order to access other pages.
						</div>

						<Form ref={ c => { this.form = c } } onSubmit={this.handleSubmit}>
							<div className="form-group">
								<Input 
									type='text' 
									name='full_name' 
									placeholder='*Full Name' 
									className='form-control' 
									value={this.state.full_name}
									onChange={this.handleChange} 
									onFocus={this.removeErrorMsg} 
									ref={c => { this.userInput = c }} 
									validations={[required, fullName]}
								/>
							</div>

							<div className="form-group">
								<Input 
									type='text' 
									name='iban' 
									placeholder='*IBAN' 
									className='form-control' 
									value={this.state.iban}
									onChange={this.handleChange} 
									validations={[required, iban]} 
								/>
							</div>

							<div className="form-group">
								<Input 
									type='text' 
									name='organization' 
									placeholder='*Organization' 
									className='form-control' 
									value={this.state.organization}
									onChange={this.handleChange} 
									validations={[required]} 
								/>
							</div>

							<div className="form-group">
								<Input 
									type='text' 
									name='address' 
									placeholder='*Address' 
									className='form-control' 
									value={this.state.address}
									onChange={this.handleChange} 
									validations={[required]} 
								/>
							</div>

							<div className="form-group">
								<Input 
									type='text' 
									name='location' 
									placeholder='*Location' 
									className='form-control' 
									value={this.state.location}
									onChange={this.handleChange} 
									validations={[required]}
								/>
							</div>

							<div className="form-group">
								<Input 
									type='text' 
									name='model_number' 
									placeholder='Model number' 
									className='form-control' 
									value={this.state.model_number}
									onChange={this.handleChange} 
								/>
							</div>

							<div className="form-group">
								<Input 
									type='text' 
									name='reference_call' 
									placeholder='Reference call' 
									className='form-control' 
									value={this.state.reference_call}
									onChange={this.handleChange} 
								/>
							</div>

							<Button type='submit' className='btn btn-dark btn-block'>Save</Button>
						</Form>
					</div>
				</div>
			);
		}
	}
}

const PaymentIssuerForm = withRouter(connect(mapStateToProps, mapDispatchToProps)(ConnectedForm));

PaymentIssuerForm.propTypes = {
  paymentIssuerActions: PropTypes.object
};

export default PaymentIssuerForm;
