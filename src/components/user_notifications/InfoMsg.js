import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';

function mapStateToProps(state) {
	return {
		notification: state.notification
	}
}

class ConnectedComponent extends Component {
	render() {
		return (
	  	<div className='col-md-12'>
	  		{ (this.props.notification.msg && this.props.notification.msg !== '') ? 
			  	<div className="alert alert-info" role="alert">
			  		{this.props.notification.msg}
					</div>
				:
					<div></div>
				}
			</div>
	  );
	}
}

const InfoMsg = connect(mapStateToProps, {})(ConnectedComponent);

InfoMsg.propTypes = {
  notification: PropTypes.object
};

export default InfoMsg;