import React from 'react';
import PropTypes from 'prop-types';

const ErrorMsg = (props) => {
	return (
  	<div className='col-md-12'>
  		{ (props.error && props.error !== '') ? 
		  	<div className="alert alert-danger" role="alert">
		  		{props.error}
				</div>
			:
				<div></div>
			}
		</div>
  ); 
}

ErrorMsg.propTypes = {
  error: PropTypes.string
};

export default ErrorMsg;