import React from 'react';
import PropTypes from 'prop-types';

const PaymentTableRow = (props) => {
	var payment = props.payment;

  return (
    <div className='row-div'>
      <div className='row-cell'>{ payment.recipient_full_name || payment.full_name }</div>
      <div className='row-cell'>{ payment.recipient_iban || payment.iban }</div>
      <div className='row-cell'>{ payment.recipient_address || payment.address }</div>
      <div className='row-cell'>{ payment.recipient_location || payment.location }</div>
      <div className='row-cell'>{ payment.recipient_model_number || payment.model_number }</div>
      <div className='row-cell'>{ payment.recipient_reference_call || payment.reference_call }</div>
      <div className='row-cell'>{ payment.amount }</div>
      <div className='row-cell large'>{ payment.description}</div>
      <div className='row-cell small'>
        <label className='clickable'>
          <input type="checkbox" 
            name="selected_payments[]" 
            value={payment.id} 
            checked={props.checked} 
            onChange={props.onChange.bind(this, payment)} 
          />
        </label>
      </div>
    </div>
  );
}

PaymentTableRow.propTypes = {
  payment: PropTypes.object,
  checked: PropTypes.bool,
  onChange: PropTypes.func,
}

export default PaymentTableRow;