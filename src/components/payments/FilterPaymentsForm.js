import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CustomDayPickerInput from '../common/CustomDayPickerInput';

class FilterPaymentsForm extends Component {
	constructor(props) {
		super(props);

		this.handleChange = this.props.handleChange.bind(this);
		this.handleDateFromChange = this.props.handleDateFromChange.bind(this);
		this.handleDateToChange = this.props.handleDateToChange.bind(this);
		this.handleKeyPress = this.props.handleKeyPress.bind(this);
	}

	render() {
		return (
	  	<form className="form-inline" id="filter-payments-form">
	    	<input 
	        type='text' 
	        name='recipient_full_name' 
	        placeholder='Full Name' 
	        className='form-control form-control-sm' 
	        onChange={this.handleChange} 
	        onKeyPress={this.handleKeyPress} 
	      />

	    	<input 
	        type='text' 
	        name='recipient_iban' 
	        placeholder='IBAN' 
	        className='form-control form-control-sm' 
	        onChange={this.handleChange} 
	        onKeyPress={this.handleKeyPress} 
	      />

	    	<CustomDayPickerInput 
	    		id="date_from" 
	    		onDayChange={this.handleDateFromChange} 
	    	/>
	    	<CustomDayPickerInput 
	    		id="date_to" 
	    		onDayChange={this.handleDateToChange} 
	    	/>

	    	<input 
	        type='number' 
	        name='amount_from' 
	        placeholder='Amount from' 
	        className='form-control form-control-sm' 
	        onChange={this.handleChange} 
	        onKeyPress={this.handleKeyPress} 
	      />

	    	<input 
	        type='number' 
	        name='amount_to' 
	        placeholder='Amount to' 
	        className='form-control form-control-sm' 
	        onChange={this.handleChange} 
	        onKeyPress={this.handleKeyPress} 
	      />
	    </form>
	  )
	}
}

FilterPaymentsForm.propTypes = {
  handleChange: PropTypes.func,
	handleDateFromChange: PropTypes.func,
	handleDateToChange: PropTypes.func,
	handleKeyPress: PropTypes.func
}

export default FilterPaymentsForm;