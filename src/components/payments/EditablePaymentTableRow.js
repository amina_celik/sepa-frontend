import React from 'react';
import PropTypes from 'prop-types';

const EditablePaymentTableRow = (props) => {
	var payment = props.payment;
  var id = payment.id;

  return (
    <div className='row-div'>
      <div className='row-cell'>{ payment.recipient_full_name || payment.full_name }</div>
      <div className='row-cell'>{ payment.recipient_iban || payment.iban }</div>
      <div className='row-cell'>{ payment.recipient_address || payment.address }</div>
      <div className='row-cell'>{ payment.recipient_location || payment.location }</div>
      <div className='row-cell'>
        <input type='text' name='model_number' className='form-control form-control-sm' defaultValue = {payment.recipient_model_number || payment.model_number} onChange={(e) => props.inputChange(e, id)} />
      </div>
      <div className='row-cell'>
        <input type='text' name='reference_call' className='form-control form-control-sm' defaultValue = {payment.recipient_reference_call || payment.reference_call} onChange={(e) => props.inputChange(e, id)} />
      </div>
      <div className='row-cell'>
        <input type='text' name='amount' className='form-control form-control-sm' defaultValue = {payment.amount} onChange={(e) => props.inputChange(e, id)} />
      </div>
      <div className='row-cell large'>
        <input type='text' name='description' className='form-control form-control-sm' defaultValue = {payment.description} onChange={(e) => props.inputChange(e, id)} />
      </div>
      <div className='row-cell small'>
        <button className='btn btn-default btn-xs' onClick={(e) => props.removeItem(e, id)}>Remove</button>
      </div>
    </div>
  );
}

EditablePaymentTableRow.propTypes = {
  payment: PropTypes.object,
  inputChange: PropTypes.func,
  removeItem: PropTypes.func
}

export default EditablePaymentTableRow;