import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as paymentsActions from '../../actions/paymentsActions';
import { withRouter, Redirect } from 'react-router-dom';

import PaymentTableRow from './PaymentTableRow';
import PaymentsTableHeader from './PaymentsTableHeader';
import Pagination from '../common/Pagination';

import { subArray, arraysOfObjectsSame } from '../../utils/ArrayUtils';

function mapStateToProps(state) {
	return { 
		payments: state.payments.payments,
		totalPayments: state.payments.total,
		paymentIssuers: state.paymentIssuers
	};
}

function mapDispatchToProps(dispatch) {
	return {
		paymentsActions: bindActionCreators(paymentsActions, dispatch)
	};
}

class ConnectedTable extends Component {
	constructor(props) {
		super(props);

		this.state = {
			currentPage: 0,
			perPage: 10
		}

		this.fetchData = this.fetchData.bind(this);
		this.changePage = this.changePage.bind(this);
		this.changedCheckbox = this.changedCheckbox.bind(this);
		this.selectPayment = this.props.selectPayment.bind(this);
		this.deselectPayment = this.props.deselectPayment.bind(this);
		this.selectAllPayments = this.props.selectAllPayments.bind(this);
		this.deselectAllPayments = this.props.deselectAllPayments.bind(this);
	}

	fetchData(filters) {
		const params = {...this.state, ...filters};
		this.props.paymentsActions.fetchPayments(params);
	}

	componentWillMount() {
		this.fetchData({});
	}

	shouldComponentUpdate(newProps, newState) {
		return (newProps.payments.length !== this.props.payments.length) || 
					 (!arraysOfObjectsSame(newProps.payments, this.props.payments)) ||
					 (newProps.selectedPayments.length !== this.props.selectedPayments.length) ||
					 (newProps.filters !== this.props.filters);
	}

	componentWillUpdate(newProps) {
		this.fetchData(newProps.filters);
	}

	changePage(newPage) {
		this.setState({ currentPage: newPage }, () => {
			this.fetchData({});
		});
	}

	changedCheckbox = (payment, event) => {
		const checked = event.target.checked;
		if(checked) {
			this.selectPayment(payment);
		}
		else {
			this.deselectPayment(payment.id);
		}
	}

	toggleAll = (event) => {
		const checked = event.target.checked;
		if(checked) {
			this.selectAllPayments(this.props.payments);
		}
		else {
			this.deselectAllPayments();
		}
	}

	render() {
		var paymentIds = this.props.payments.map((a) => { return a.id }).sort();
		var selectedPaymentIds = this.props.selectedPayments.map((a) => { return a.id }).sort();
		var selectedAll = subArray(paymentIds, selectedPaymentIds);

		if(!this.props.payments) {
			return (
				<div> Loading payment details ... </div>
			);
		}
		else if(this.props.paymentIssuers.length === 0) {
			return <Redirect to="/payment_issuers/new" />;
		}
		else {
			if(this.props.payments.length === 0) {
				return(
					<div>No payments to show. To generate payments, please click on 'Generate Sepa File' button.</div>
				)
			}
			else {
				return (
					<div className='payments-container-div'>
						<PaymentsTableHeader 
							selectable={true}
							toggleAll={this.toggleAll} 
							selectedAll={selectedAll} 
						/>

						{
							this.props.payments.map((item, index) => {
	              return (
	                <PaymentTableRow 
	                	payment = { item } 
	                	key = { item.id } 
	                	checked = { this.props.selectedPayments.some(paym => paym.id === item.id) } 
	                	onChange = { this.changedCheckbox } 
	                />
	              );
	            })
						}

						<Pagination totalCount={this.props.totalPayments} perPage={this.state.perPage} currentPage={this.state.currentPage} changePage={this.changePage} />
		  		</div>
				);
			}
		}
	}
}

const PaymentsTable = withRouter(connect(mapStateToProps, mapDispatchToProps)(ConnectedTable));

PaymentsTable.propTypes = {
  paymentsActions: PropTypes.object,
  payments: PropTypes.array,
  selectedPayments: PropTypes.array,
  filters: PropTypes.object,
  changedCheckbox: PropTypes.func,
	selectPayment: PropTypes.func,
	deselectPayment: PropTypes.func,
	selectAllPayments: PropTypes.func,
	deselectAllPayments: PropTypes.func
};

export default PaymentsTable;