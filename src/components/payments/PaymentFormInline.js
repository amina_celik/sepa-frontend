import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Button from 'react-validation/build/button';

import { required, iban, fullName, decimal } from '../../utils/Validators';

class PaymentFormInline extends Component {
	constructor() {
		super();

		this.state = {
			date: '',
			amount: '',
			currency: 'USD',
			description: '',
			recipient_full_name: '',
			recipient_iban: '',
			recipient_address: '',
			recipient_location: '',
			recipient_model_number: '',
			recipient_reference_call: ''
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		});
	}

	handleSubmit(event) {
		event.preventDefault();
		this.form.validateAll();

		this.props.addSepaPayment(this.state);

		this.setState({
			date: '',
			amount: '',
			description: '',
			recipient_full_name: '',
			recipient_iban: '',
			recipient_address: '',
			recipient_location: '',
			recipient_model_number: '',
			recipient_reference_call: ''
		});
	}

	removeErrorMsg = () => {
		this.form.hideError(this.userInput);
	}

	render() {
		return (
			<div className='row-div'>
				<Form ref={ c => { this.form = c } } onSubmit={this.handleSubmit}>
					<div className="row-cell">
						<Input 
							type='text' 
							name='recipient_full_name' 
							placeholder='*Full Name' 
							className='form-control form-control-sm' 
							value={this.state.recipient_full_name}
							onChange={this.handleChange} 
							onFocus={this.removeErrorMsg} 
							ref={c => { this.userInput = c }} 
							validations={[required, fullName]}
						/>
					</div>

					<div className="row-cell">
						<Input 
							type='text' 
							name='recipient_iban' 
							placeholder='*IBAN' 
							className='form-control form-control-sm' 
							value = {this.state.recipient_iban} 
							onChange={this.handleChange} 
							validations={[required, iban]} />
					</div>

					<div className="row-cell">
						<Input 
							type='text' 
							name='recipient_address' 
							placeholder='*Address' 
							className='form-control form-control-sm' 
							value = {this.state.recipient_address} 
							onChange={this.handleChange} 
							validations={[required]} 
						/>
					</div>

					<div className="row-cell">
						<Input 
							type='text' 
							name='recipient_location' 
							placeholder='*Location' 
							className='form-control form-control-sm' 
							value = {this.state.recipient_location} 
							onChange={this.handleChange} 
							validations={[required]} 
						/>
					</div>

					<div className="row-cell">
						<Input 
							type='text' 
							name='recipient_model_number' 
							placeholder='Model number' 
							className='form-control form-control-sm' 
							value = {this.state.recipient_model_number} 
							onChange={this.handleChange} 
						/>
					</div>

					<div className="row-cell">
						<Input 
							type='text' 
							name='recipient_reference_call' 
							placeholder='*Reference Call' 
							className='form-control form-control-sm' 
							value = {this.state.recipient_reference_call} 
							onChange={this.handleChange} 
							validations={[required]} 
						/>
					</div>

					<div className="row-cell">
						<Input 
							type='number' 
							name='amount' 
							placeholder='*Amount' 
							className='form-control form-control-sm' 
							value = {this.state.amount} 
							onChange={this.handleChange} 
							validations={[required, decimal]} 
						/>
					</div>

					<div className="row-cell large">
						<Input 
							type='text' 
							name='description' 
							placeholder='*Description' 
							className='form-control form-control-sm' 
							value = {this.state.description} 
							onChange={this.handleChange} 
							validations={[required]} 
						/>
					</div>

					<div className="row-cell small">
						<Button 
							type='submit' className='btn btn-dark btn-block btn-sm'>Add</Button>
					</div>
				</Form>
			</div>
		)
	}
}

PaymentFormInline.propTypes = {
  addSepaPayment: PropTypes.func
}

export default PaymentFormInline;
