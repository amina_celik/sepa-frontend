import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as sepaPaymentsActions from '../../actions/sepaPaymentsActions';
import { withRouter, Redirect } from 'react-router-dom';

import PaymentsTableHeader from './PaymentsTableHeader';
import EditablePaymentTableRow from './EditablePaymentTableRow';
import PaymentFormInline from './PaymentFormInline';

var shortid = require('shortid');

function mapStateToProps(state) {
	return {
		sepaPayments: state.sepaPayments,
		redirectToDashboard: false
	}
}

function mapDispatchToProps(dispatch) {
	return {
		sepaPaymentsActions: bindActionCreators(sepaPaymentsActions, dispatch)
	};
};

class ConnectedEditablePaymentsTable extends Component {
	constructor(props) {
		super(props);

		this.state = {
			sepaPayments: this.props.selectedPayments
		}

		this.handleSubmit = this.handleSubmit.bind(this);
		this.handleInputChange = this.handleInputChange.bind(this);
	}

	addItem = (payment) => {
		this.setState({
			sepaPayments: this.state.sepaPayments.concat([payment])
		});
	}

	removeItem = (event, paymentId) => {
		var sepaPayments = [...this.state.sepaPayments];
		var index = sepaPayments.findIndex(function(obj) {
			return obj.id === paymentId;
		});

		sepaPayments.splice(index, 1);
		this.setState({ sepaPayments: sepaPayments });
	}

	handleInputChange(event, paymentId) {
		var sepaPayments = [...this.state.sepaPayments];
		var index = sepaPayments.findIndex(function(obj) {
			return obj.id === paymentId;
		});

		sepaPayments[index][event.target.name] = event.target.value;
		this.setState({ sepaPayments: sepaPayments });
	}

	handleSubmit(event) {
		event.preventDefault();

		const sepaPayments = Object.assign({}, this.state.sepaPayments);
		this.props.sepaPaymentsActions.bulkCreatePayments({ sepaPayments });

		this.setState({ 
			sepaPayments: [], 
			redirectToDashboard: true 
		});
	}

	render() {
		if(this.state.redirectToDashboard) {
			return <Redirect to="/dashboard" />;
		}
		else {
			return (
				<div>

					<div style={{'overflow': 'hidden' }}>
						<div className="pull-right">
	            <button className='btn btn-dark btn-block' onClick={this.handleSubmit}>Finish</button>
	          </div>
	        </div>

					<div className='payments-container-div'>
						<PaymentsTableHeader />

				    <PaymentFormInline addSepaPayment={this.addItem} key={shortid.generate()}/>

						{
							this.state.sepaPayments.map((item, index) => {
		            return (
		            	<EditablePaymentTableRow 
		            			payment = { item } 
		            			key = { item.id || index } 
		            			removeItem={this.removeItem} 
		            			inputChange={this.handleInputChange} 
		            		/>
		            );
		          })
						}
		  		</div>
		  	</div>
			);
		}
	}
}

const EditablePaymentsTable = withRouter(connect(mapStateToProps, mapDispatchToProps)(ConnectedEditablePaymentsTable));

EditablePaymentsTable.propTypes = {
	sepaPaymentsActions: PropTypes.object,
	selectedPayments: PropTypes.array
}

export default EditablePaymentsTable;