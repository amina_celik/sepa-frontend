import React from 'react';
import PropTypes from 'prop-types';

const PaymentTableHeader = (props) => {
  return (
  	<div className='header-div row-div'>
      <div className='row-cell'>Recipient Full Name</div>
      <div className='row-cell'>Recipient IBAN</div>
      <div className='row-cell'>Recipient Address</div>
      <div className='row-cell'>Recipient Location</div>
      <div className='row-cell'>Recipient Model Number</div>
      <div className='row-cell'>Recipient Reference Call</div>
      <div className='row-cell'>Amount</div>
      <div className='row-cell large'>Description</div>
      <div className='row-cell small'>
        { props.selectable ?
          <label className='clickable header-label'>
            <input type="checkbox" 
            name="selected_payments[]" 
            checked={props.selectedAll} 
            onChange={props.toggleAll.bind(this)} 
            /> All
          </label>
        :
          <div></div>
        }
      </div>
    </div>
  );
}

PaymentTableHeader.propTypes = {
  selectedAll: PropTypes.bool,
  toggleAll: PropTypes.func,
}

export default PaymentTableHeader;