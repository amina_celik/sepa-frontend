import React, { Component } from 'react';
import PropTypes from 'prop-types';

class FilterRecipientsForm extends Component {
	constructor(props) {
		super(props);

		this.handleChange = this.props.handleChange.bind(this);
		this.handleKeyPress = this.props.handleKeyPress.bind(this);
	}

	render() {
		return (
	  	<form className="form-inline" id="filter-recipients-form">
	    	<input 
	        type='text' 
	        name='recipient_full_name' 
	        placeholder='Full Name' 
	        className='form-control form-control-sm' 
	        onChange={this.handleChange} 
	        onKeyPress={this.handleKeyPress} 
	      />

	    	<input 
	        type='text' 
	        name='recipient_iban' 
	        placeholder='IBAN' 
	        className='form-control form-control-sm' 
	        onChange={this.handleChange} 
	        onKeyPress={this.handleKeyPress} 
	      />
	    </form>
	  )
	}
}

FilterRecipientsForm.propTypes = {
	handleChange: PropTypes.func,
	handleKeyPress: PropTypes.func
}

export default FilterRecipientsForm;