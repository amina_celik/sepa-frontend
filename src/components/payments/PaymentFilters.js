import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FilterPaymentsForm from './FilterPaymentsForm';
import FilterRecipientsForm from './FilterRecipientsForm';

class PaymentFilters extends Component {
  constructor() {
    super();

    this.state = {
      recipient_full_name: '',
      recipient_iban: '',
      date_from: '',
      date_to: '',
      amount_from: '',
      amount_to: '',
      filterType: 'payments'
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleDateFromChange = this.handleDateFromChange.bind(this);
    this.handleDateToChange = this.handleDateToChange.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }
  
  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleDateFromChange(selectedDay) {
    this.setState({
      date_from: selectedDay
    }, () => {
      this.props.parentCallback(this.state);
    })
  }

  handleDateToChange(selectedDay) {
    this.setState({
      date_to: selectedDay
    }, () => {
      this.props.parentCallback(this.state);
    })
  }

  handleKeyPress(event) {
    if(event.key === 'Enter') {
      this.props.parentCallback(this.state);
    }
  }

  toggleCriteria = (event) => {
    this.setState({ 
      filterType: event.target.id,
      recipient_full_name: '',
      recipient_iban: '',
      date_from: '',
      date_to: '',
      amount_from: '',
      amount_to: ''
    })
  }

  render () {
    return (
    	<div className='mb-5'>
    		<div className="row mt-5 ml-1">
    			<label className="pt-2 mr-3">Filtering criteria:</label>

  	  		<div className="btn-group btn-group-toggle" data-toggle="buttons">
  				  <label className={ "btn btn-info " + (this.state.filterType === 'payments' ? 'active' : '') }>
  				    <input type="radio" name="options" id="payments" autoComplete="off" defaultChecked onClick={this.toggleCriteria} /> Payments
  				  </label>
  				  <label className={ "btn btn-info " + (this.state.filterType === 'recipients' ? 'active' : '') }>
  				    <input type="radio" name="options" id="recipients" autoComplete="off" onClick={this.toggleCriteria} /> Recipients
  				  </label>
  				</div>
  			</div>

        { 
          this.state.filterType === 'payments' ?
            <FilterPaymentsForm 
              handleChange={this.handleChange} 
              handleDateFromChange={this.handleDateFromChange}
              handleDateToChange={this.handleDateToChange}
              handleKeyPress={this.handleKeyPress} 
            /> :
            <FilterRecipientsForm 
              handleChange={this.handleChange} 
              handleKeyPress={this.handleKeyPress} 
            />
        }

        <hr />
      </div>
    );
  }
}

PaymentFilters.propTypes = {
  parentCallback: PropTypes.func
}

export default PaymentFilters;