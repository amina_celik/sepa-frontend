export const subArray = (potentialSubArray, array) => {
  var presenceArr = potentialSubArray.map((elem) => {
  	return array.some(arrElem => arrElem === elem);
  });

  // get unique true/false values
  presenceArr = presenceArr.filter((val, id, array) => {
	  return array.indexOf(val) === id;  
	});

  return presenceArr.length === 1 && presenceArr[0] === true;
};

export const arraysOfObjectsSame = (array1, array2) => {
	var ids1 = array1.map((el) => { return el.id }).sort();
	var ids2 = array2.map((el) => { return el.id }).sort();

	return ids1.join(';') === ids2.join(';');
}