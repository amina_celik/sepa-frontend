import React from 'react';
import ValidationErrorMsg from '../components/common/ValidationErrorMsg';

export const required = (value) => {
  if (!value.toString().trim().length) {
    return (
    	<ValidationErrorMsg msg='Required field.' />
    );
  }
};

export const exactLength = (value, props) => {
  if (value.toString().trim().length !== props.exactLength) {
    return (
    	<ValidationErrorMsg msg="The value should be {props.exactLength} characters long." />
    )
  }
};

export const iban = (value, props) => {
	let val = value.toString().trim();
	let pattern = /^[A-Z]{2}\d{19}$/;

	if(!pattern.test(val)) {
		return (
    	<ValidationErrorMsg msg="IBAN is not valid." />
    )
	}
};

export const fullName = (value, props) => {
	let val = value.toString().trim();
	let pattern = /^([A-Za-z]{2,})(\s+[A-Za-z]{2,})+$/;

	if(!pattern.test(val)) {
		return (
			<ValidationErrorMsg msg="This doesn't look like a valid full name." />
		)
	}
};

export const decimal = (value, props) => {
	let val = value.toString().trim();
	let pattern = /^\s*-?\d+(\.\d{1,2})?\s*$/;

	if(!pattern.test(val)) {
		return (
			<ValidationErrorMsg msg="Amount should be numeric with up to 2 decimals." />
		)
	}
};

export const password = (value, props) => {
	let val = value.toString().trim();
	let pattern = /^(?=.{8,})(?=.*[0-9])(?=.*[ !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])/;

	if(!pattern.test(val)) {
		return (
			<ValidationErrorMsg msg="Password must contain at least one digit, one special character and be more than 8 characters long." />
		)
	}
}

export const passwordConfirmation = (value, props, components) => {
  const bothUsed = components.password[0].isUsed && components.password_confirmation[0].isUsed;
  const bothChanged = components.password[0].isChanged && components.password_confirmation[0].isChanged;

  if (bothChanged && bothUsed && components.password[0].value !== components.password_confirmation[0].value) {
    return <ValidationErrorMsg msg="Password and password comfirmation are not equal." />;
  }
};
