import React, { Component } from 'react';
import EditablePaymentsTable from '../components/payments/EditablePaymentsTable';

class GenerateSepaFilePage extends Component {
	render() {
		return (
	  	<div>
	  		<h2>Generate new SEPA compatible file</h2>
	  		<EditablePaymentsTable />
	  	</div>
	  );
	}
}

export default GenerateSepaFilePage;