import React, { Component } from 'react';

class HomePage extends Component {
	render() {
		return (
	  	<div>
	  		<h2>Generate SEPA compatible XML files for mass payments</h2>

	  		<div className="row">
		  		<div className="col-md-6">
		  			Those XML files are then usually imported into banking application or web application, in order to do multiple payments at once.
		  		</div>
		  	</div>
	  	</div>
	  );
	}
  
}

export default HomePage;