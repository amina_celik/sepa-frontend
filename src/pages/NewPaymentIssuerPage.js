import React from 'react';
import PaymentIssuerForm from '../components/issuers/PaymentIssuerForm';

const NewPaymentIssuerPage = (props) => {
  return (
  	<div>
  		<PaymentIssuerForm />
  	</div>
  )
}

export default NewPaymentIssuerPage;