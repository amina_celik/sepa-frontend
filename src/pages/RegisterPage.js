import React from 'react';
import RegisterUserForm from '../components/users/RegisterUserForm';

const RegisterPage = (props) => {
  return (
  	<div>
  		<RegisterUserForm />
  	</div>
  )
}

export default RegisterPage;