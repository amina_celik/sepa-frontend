import React from 'react';
import ForgotPasswordForm from '../components/users/ForgotPasswordForm';

const ForgotPasswordPage = (props) => {
  return (
  	<div>
  		<ForgotPasswordForm />
  	</div>
  )
}

export default ForgotPasswordPage;