import React, { Component } from 'react';
import PaymentsTable from '../components/payments/PaymentsTable';
import PaymentFilters from '../components/payments/PaymentFilters';
import EditablePaymentsTable from '../components/payments/EditablePaymentsTable';

class FilterPaymentsContainer extends Component {
	constructor() {
		super();

		this.state = {
			filters: {},
			selectedPayments: [],
			generateSepaFile: false
    }

		this.notifyOfAppliedFilter = this.notifyOfAppliedFilter.bind(this);
		this.generateSepaFileAction = this.generateSepaFileAction.bind(this);
		this.selectPayment = this.selectPayment.bind(this);
		this.deselectPayment = this.deselectPayment.bind(this);
	}
	
	notifyOfAppliedFilter = (filters) => {
		this.setState({ filters: filters });
	}

	generateSepaFileAction = () => {
		this.setState({ generateSepaFile: true });
	}

	selectPayment = (payment) => {
		this.setState({
			selectedPayments: this.state.selectedPayments.concat([payment])
		});
	}

	deselectPayment = (paymentId) => {
		var selectedPayments = [...this.state.selectedPayments];
		var index = selectedPayments.findIndex(function(obj) {
			return obj.id === paymentId;
		});

		selectedPayments.splice(index, 1);
		this.setState({ selectedPayments: selectedPayments });
	}

	selectAllPayments = (allPayments) => {
		this.setState({ selectedPayments: allPayments });
	}

	deselectAllPayments = () => {
		this.setState({ selectedPayments: [] });
	}

	render() {
		if(this.state.generateSepaFile) {
			return (
				<div>
					<h2>Generate SEPA compatible file</h2>

					<EditablePaymentsTable selectedPayments={this.state.selectedPayments} />
				</div>
			);
		}
		else {
			return (
		  	<div>
		  		<h2>Payments History</h2>

		  		<div className="pull-right">
            <button className='btn btn-dark btn-block' onClick={this.generateSepaFileAction}>Generate SEPA file</button>
          </div>

		  		<PaymentFilters parentCallback={this.notifyOfAppliedFilter} />
		  		
		  		<PaymentsTable 
		  			filters={this.state.filters} 
		  			selectedPayments={this.state.selectedPayments}
		  			selectPayment={this.selectPayment} 
		  			deselectPayment={this.deselectPayment}  
		  			selectAllPayments={this.selectAllPayments}
		  			deselectAllPayments={this.deselectAllPayments}
		  		/>
		  	</div>
		  );
		}
	}
}

export default FilterPaymentsContainer;