import React from 'react';
import LoginForm from '../components/users/LoginForm';

const LoginPage = (props) => {
  return (
  	<div>
  		<LoginForm />
  	</div>
  )
}

export default LoginPage;