import React from 'react';
import ConfirmEmailForm from '../components/users/ConfirmEmailForm';

const ConfirmEmailPage = (props) => {
  return (
  	<div>
  		<ConfirmEmailForm />
  	</div>
  )
}

export default ConfirmEmailPage;