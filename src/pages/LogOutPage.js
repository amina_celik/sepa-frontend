import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { logoutUser } from '../actions/userActions';
import { Redirect } from 'react-router-dom';

class LogOutPage extends Component {
	componentWillMount() {
		this.props.dispatch(logoutUser());
		this.props.history.push('/login');
	}

	render() {
		return <Redirect to="/" />;
	}
}

export default withRouter(connect()(LogOutPage));