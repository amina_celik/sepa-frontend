import React, { Component } from 'react';
import PaymentIssuersList from '../components/issuers/PaymentIssuersList';
import { Link } from 'react-router-dom';

class DashboardPage extends Component {
	render() {
		return (
	  	<div>
	  		<h2>Dashboard</h2>

	  		<div className="row">
		  		<div className="col-md-6">
		  			<PaymentIssuersList />
		  		</div>

		  		<div className="col-md-6 new-issuer-link-container">
		  			<Link to="/payment_issuers/new" className="btn btn-info new-issuer-link">Create new payment issuer data set</Link>
		  		</div>
		  	</div>
	  	</div>
	  );
	}
  
}

export default DashboardPage;