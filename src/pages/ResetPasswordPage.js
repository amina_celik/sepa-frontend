import React from 'react';
import ResetPasswordForm from '../components/users/ResetPasswordForm';

const ResetPasswordPage = (props) => {
  return (
  	<div>
  		<ResetPasswordForm />
  	</div>
  )
}

export default ResetPasswordPage;