// GET /api/v1/payment_issuers
export const FETCH_PAYMENT_ISSUERS = "FETCH_PAYMENT_ISSUERS";
export const RECEIVE_PAYMENT_ISSUERS = "RECEIVE_PAYMENT_ISSUERS";

// GET /api/v1/payments
export const FETCH_PAYMENTS = "FETCH_PAYMENTS";
export const RECEIVE_PAYMENTS = "RECEIVE_PAYMENTS";

// POST /api/v1/payment_issuers
export const CREATE_PAYMENT_ISSUER_SUCCESS = "CREATE_PAYMENT_ISSUER_SUCCESS";

// POST /api/v1/payments
export const CREATE_PAYMENT_SUCCESS = "CREATE_PAYMENT_SUCCESS";

// POST /api/v1/payments/bulk_create
export const CREATE_PAYMENTS_SUCCESS = "CREATE_PAYMENTS_SUCCESS";

// POST /api/v1/users
export const CREATE_USER_SUCCESS = "CREATE_USER_SUCCESS";

// POST /api/v1/users/log_in
export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";
export const LOGIN_USER_ERROR = "LOGIN_USER_ERROR";

// POST /api/v1/users/forgot_password
export const FORGOT_PASSWORD_SUCCESS = "FORGOT_PASSWORD_SUCCESS";

// POST /api/v1/users/reset_password
export const RESET_PASSWORD_SUCCESS = "RESET_PASSWORD_SUCCESS";

// POST /api/v1/users/confirmation
export const CONFIRM_EMAIL_SUCCESS = "CONFIRM_EMAIL_SUCCESS";

export const SHOW_NOTIFICATION = "SHOW_NOTIFICATION";
export const HIDE_NOTIFICATION = "HIDE_NOTIFICATION";