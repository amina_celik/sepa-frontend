import { RECEIVE_PAYMENTS } from "../constants/actionTypes";
import { sessionService } from 'redux-react-session';

const queryString = require('query-string');

function url(params) {
	let baseUrl = process.env.REACT_APP_BACKEND_URL + '/api/v1/payments?';
	let joinedParams = queryString.stringify(params);
	return baseUrl + joinedParams;
}

export function recievePayments(json) {
	return { type: RECEIVE_PAYMENTS, payments: json.payments, total: json.total };
}

function fetchPaymentsWithToken(params, tokenContainer) {
	const token = tokenContainer.info.auth_token;
	return fetch(url(params), {
		method: 'GET',
		mode: 'cors',
		headers: {
			'Accept': 'application/json',
			'Authorization': token
		}
	})
}

export function fetchPayments(params) {
	return function(dispatch) {
		return sessionService.loadSession()
			.then(tokenContainer => fetchPaymentsWithToken(params, tokenContainer))
			.then(response => response.json())
			.then(json => dispatch(recievePayments(json)))
			.catch(error => { throw(error) });
	}
}