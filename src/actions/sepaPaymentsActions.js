import { CREATE_PAYMENTS_SUCCESS } from "../constants/actionTypes";
import { sessionService } from 'redux-react-session';
import showNotification from './notificationActions';
import Axios from 'axios';

var fileDownload = require('react-file-download');

function bulkCreatePaymentsWithToken(payments, token) {
	return Axios.post(process.env.REACT_APP_BACKEND_URL + '/api/v1/payments/bulk_create', payments, {
		headers: {
			'Authorization': token
		}
	});
}

function downloadPaymentsWithToken(filePath, token) {
	return Axios.get(process.env.REACT_APP_BACKEND_URL + '/api/v1/payments/download_file?file_path=' + filePath, {
		headers: {
			'Authorization': token
		}
	});
}

export function bulkCreatePayments(payments) {
	// console.log('These are submitted payments: ');
	// console.log(payments);

	var token;

	return function(dispatch) {
		return sessionService.loadSession()
		.then(tokenContainer => {
			token = tokenContainer.info.auth_token;
			dispatch(showNotification('Payments are successfully created!'));
			return bulkCreatePaymentsWithToken(payments, token);
		})
		.then(response => downloadPaymentsWithToken(response.data.file_path, token))
		.then(downloadResp => {
			fileDownload(downloadResp.data, 'sepaCompatibleFile.xml');
			dispatch(showNotification("Your SEPA compatible file is downloaded! Please check your browser's download location."));
			return dispatch(createPaymentsSuccess());
		})
		.catch(error => { throw(error); });
	};
}

export function createPaymentsSuccess() {
	return { type: CREATE_PAYMENTS_SUCCESS }
}