import { RECEIVE_PAYMENT_ISSUERS } from "../constants/actionTypes";
import { sessionService } from 'redux-react-session';

export function recievePaymentIssuers(json) {
	return { type: RECEIVE_PAYMENT_ISSUERS, paymentIssuers: json.paymentIssuers };
}

function fetchPaymentIssuersWithToken(tokenContainer) {
	const token = tokenContainer.info.auth_token;
	return fetch(process.env.REACT_APP_BACKEND_URL + '/api/v1/payment_issuers', {
		method: 'GET',
		mode: 'cors',
		headers: {
			'Accept': 'application/json',
			'Authorization': token
		}
	});
}

export function fetchPaymentIssuers() {
	return function(dispatch) {
		return sessionService.loadSession()
			.then(tokenContainer => fetchPaymentIssuersWithToken(tokenContainer))
			.then(response => response.json())
			.then(json => dispatch(recievePaymentIssuers(json)))
			.catch(error => { throw(error) });
	}
}