import Axios from 'axios';
import { CREATE_PAYMENT_ISSUER_SUCCESS } from "../constants/actionTypes";
import { sessionService } from 'redux-react-session';
import showNotification from './notificationActions';

export function createPaymentIssuer(issuer) {
	return function(dispatch) {
		return sessionService.loadSession()
			.then(tokenContainer => createPaymentIssuerWithToken(issuer, tokenContainer))
			.then(response => {
				dispatch(createPaymentIssuerSuccess(response.data));
				dispatch(showNotification('Payment issuer details are successfully created!'));
			})
			.catch(error => { throw(error); })
	}
}

export function createPaymentIssuerSuccess(issuer) {
	return { type: CREATE_PAYMENT_ISSUER_SUCCESS, issuer }
}

function createPaymentIssuerWithToken(issuer, tokenContainer) {
	const token = tokenContainer.info.auth_token;
	return Axios.post(process.env.REACT_APP_BACKEND_URL + '/api/v1/payment_issuers', issuer, {
		headers: {
			'Authorization': token
		}
	});
}