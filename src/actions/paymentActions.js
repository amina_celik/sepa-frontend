import Axios from 'axios';
import { CREATE_PAYMENT_SUCCESS } from "../constants/actionTypes";
import { sessionService } from 'redux-react-session';
import showNotification from './notificationActions';

function createPaymentWithToken(payment, tokenContainer) {
	const token = tokenContainer.info.auth_token;
	return Axios.post(process.env.REACT_APP_BACKEND_URL + '/api/v1/payments', payment.payment, {
		headers: {
			'Authorization': token
		}
	});
}

export function createPaymentSuccess(payment) {
	return { type: CREATE_PAYMENT_SUCCESS, payment }
}

export function createPayment(payment) {
	return function(dispatch) {
		return sessionService.loadSession()
			.then(tokenContainer => createPaymentWithToken(payment, tokenContainer))
			.then(response => {
				dispatch(createPaymentSuccess(response.data.payment));
				dispatch(showNotification('Payment is successfully created!'));
			})
			.catch(error => { throw(error); });
	};
}

