import { SHOW_NOTIFICATION, HIDE_NOTIFICATION } from "../constants/actionTypes";

export default function showNotification(msg) {
	return dispatch => {
		dispatch({ type: SHOW_NOTIFICATION, msg });

		setTimeout(() => dispatch(hideNotification()), 5000);
	}
}

export function hideNotification() {
	return dispatch => {
		dispatch({ type: HIDE_NOTIFICATION });
	}
}