import Axios from 'axios';
import { CREATE_USER_SUCCESS, LOGIN_USER_SUCCESS, RESET_PASSWORD_SUCCESS, FORGOT_PASSWORD_SUCCESS, CONFIRM_EMAIL_SUCCESS, LOGIN_USER_ERROR } from "../constants/actionTypes";
import { sessionService } from 'redux-react-session';
import showNotification from './notificationActions';

// Create User
export function createUser(user) {
	return dispatch => {
		return Axios.post(process.env.REACT_APP_BACKEND_URL + '/api/v1/users', user)
			.then(response => {
				dispatch(createUserSuccess(response.data));
				dispatch(showNotification('Your account is created! You will get confirmation instructions to your email address that you must follow in order to log in. Thanks!'));
			})
			.catch(error => {
				throw(error);
			});
	};
}

export function createUserSuccess(user) {
	return { type: CREATE_USER_SUCCESS, user }
}

// Login User
export function loginUser(user) {
	var headers= {
		'Access-Control-Expose-Headers': '*',
		'Access-Control-Allow-Origin': '*',
		'credentials': 'same-origin',
		'Content-Type': 'application/json'
	};

	return dispatch => {
		return Axios.post(process.env.REACT_APP_BACKEND_URL + '/api/v1/users/login', user, headers)
			.then(response => {
				var info = response.data;
				sessionService.saveSession({ info });
				dispatch(loginUserSuccess(info));
				dispatch(showNotification('Logged in!'));
			})
			.catch(error => {
				if(error.response) {
					var errorMsg = error.response.statusText + ': ' + error.response.data.error;
					dispatch(loginUserError(errorMsg));
				}
				else if(error.request) {
					console.log('There was a problem with request.');
				}
				else {
					console.log(error.message);
				}
			});
	};
}

export function loginUserSuccess(user) {
	return { type: LOGIN_USER_SUCCESS, user }
}

export function loginUserError(msg) {
	return { type: LOGIN_USER_ERROR, msg }
}

// Logout User
export function logoutUser() {
	return ((dispatch) => {
		dispatch(showNotification('Logged out!'));
		sessionService.deleteSession();
	});
}

// Confirm Email for User
export function confirmEmail(token) {
	return dispatch => {
		return Axios.get(process.env.REACT_APP_BACKEND_URL + '/api/v1/users/confirmation?confirmation_token=' + token)
			.then(response => {
				if(response.status === 200) {
					dispatch(confirmEmailSuccess());
					dispatch(showNotification('Your email address is now confirmed!'));
				}
			})
			.catch(error => {
				throw(error);
			});
	};
}

export function confirmEmailSuccess() {
	return { type: CONFIRM_EMAIL_SUCCESS }
}

// Forgot Password for User
export function forgotPassword(user) {
	return dispatch => {
		return Axios.post(process.env.REACT_APP_BACKEND_URL + '/api/v1/users/password', user)
			.then(response => {
				dispatch(forgotPasswordSuccess(response.data))
				dispatch(showNotification('We have sent reset password instructions to your email address.'));
			})
			.catch(error => {
				throw(error);
			});
	};
}

export function forgotPasswordSuccess(user) {
	return { type: FORGOT_PASSWORD_SUCCESS, user }
}

// Rest Password for User
export function resetPassword(user) {
	return dispatch => {
		return Axios.put(process.env.REACT_APP_BACKEND_URL + '/api/v1/users/password', user)
			.then(response => {
				dispatch(resetPasswordSuccess(response.data))
				dispatch(showNotification('Your password is changed!'));
			})
			.catch(error => {
				throw(error);
			});
	};
}

export function resetPasswordSuccess(user) {
	return { type: RESET_PASSWORD_SUCCESS, user }
}