import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducers/allReducers';
import thunk from 'redux-thunk';
import { sessionService } from 'redux-react-session';

const store = createStore(
	rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(thunk)
);

sessionService.initSessionService(store);

export default store;