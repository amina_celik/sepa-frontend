import { CONFIRM_EMAIL_SUCCESS, LOGIN_USER_ERROR, LOGIN_USER_SUCCESS } from '../constants/actionTypes';

const initialEmailState = {
	runningConfirmation: true,
	confirmed: false,
	error: ''
};

export default function user(state = initialEmailState, action) {
	switch(action.type) {
		case CONFIRM_EMAIL_SUCCESS:
			return {
				runningConfirmation: false,
				confirmed: true,
				error: ''
			};
		case LOGIN_USER_ERROR:
			return {
				runningConfirmation: false,
				confirmed: true,
				error: action.msg
			};
		case LOGIN_USER_SUCCESS:
			return {
				runningConfirmation: false,
				confirmed: true,
				error: action.msg
			}
		default:
			return state;
	}
}