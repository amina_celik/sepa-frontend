import { FETCH_PAYMENT_ISSUERS, RECEIVE_PAYMENT_ISSUERS, CREATE_PAYMENT_ISSUER_SUCCESS } from '../constants/actionTypes';

const initialState = {
  paymentIssuers: []
}

export default function paymentIssuers(state = initialState.paymentIssuers, action) {
  switch (action.type) {
    case FETCH_PAYMENT_ISSUERS:
      return action;
    case RECEIVE_PAYMENT_ISSUERS:
      return action.paymentIssuers;
    case CREATE_PAYMENT_ISSUER_SUCCESS:
      return state.concat(action.issuer.paymentIssuer);
    default:
      return state;
  }
}