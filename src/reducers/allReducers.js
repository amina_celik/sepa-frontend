import { combineReducers } from 'redux';
import paymentIssuers from './paymentIssuersReducer';
import payments from './paymentsReducer';
import user from './userReducer';
import notification from './notificationReducer';
import { sessionReducer } from 'redux-react-session';

const rootReducer = combineReducers({
  paymentIssuers,
  payments,
  user,
  notification,
  session: sessionReducer,
});

export default rootReducer;