import { FETCH_PAYMENTS, RECEIVE_PAYMENTS } from '../constants/actionTypes';

const initialState = {
  payments: [],
  total: 0
}

export default function payments(state = initialState, action) {
  switch (action.type) {
    case FETCH_PAYMENTS:
      return action;
    case RECEIVE_PAYMENTS:
      return {
        payments: action.payments,
        total: action.total
      };
    default:
      return state;
  }
}