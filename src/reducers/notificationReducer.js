import { SHOW_NOTIFICATION, HIDE_NOTIFICATION } from "../constants/actionTypes";

const initialState = {
	msg: ''
};

export default function notification(state = initialState, action) {
	switch(action.type) {
		case SHOW_NOTIFICATION:
			return {
				msg: action.msg
			};
		case HIDE_NOTIFICATION:
			return initialState;  
		default:
			return state;
	}
}