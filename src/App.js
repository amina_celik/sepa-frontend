import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router } from 'react-router-dom';
import Navigation from './components/common/Navigation';
import InfoMsg from './components/user_notifications/InfoMsg';
import Routes from './routes';
import { connect } from 'react-redux';

const App = ({ authenticated, checked }) => {
  return (
    <Router>
      <div>
      	<Navigation authenticated={authenticated} checked={checked} />

        <InfoMsg />

      	<div className="container" id="main-container">
          <Routes authenticated={authenticated} checked={checked} />
        </div>

      </div>
    </Router>
  );
}

const { bool } = PropTypes;

App.propTypes = {
  authenticated: bool.isRequired,
  checked: bool.isRequired
};

function mapStateToProps(state) {
  return {
    authenticated: state.session.authenticated,
    checked: state.session.checked
  }
}

export default connect(mapStateToProps)(App);
